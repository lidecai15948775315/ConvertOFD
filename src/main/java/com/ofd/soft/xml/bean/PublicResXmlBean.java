package com.ofd.soft.xml.bean;

import java.util.List;
import java.util.Map;

import lombok.Data;

/**
 *  
 * 公共资源序列,每一个节点指向OFD包内的资源描述文档
 * @ClassName:  PublicResXmlBean   
 * @Description:TODO(描述这个类的作用)   
 * @author: 李德才
 * @date:   2020年4月18日 下午1:09:15
 */

@Data
public class PublicResXmlBean {
	
	private List<FontBean> fontList;
	private String path;
	private Map<String,String> map;
}
