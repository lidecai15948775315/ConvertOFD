package com.ofd.soft.xml.bean;

import java.util.List;
import lombok.Data;


/**
 * 根文件
 * @ClassName:  DocumentXmlBean   
 * @Description:TODO(描述这个类的作用)   
 * @author: 李德才
 * @date:   2020年4月18日 下午1:13:14
 */

@Data 
public class DocumentXmlBean {
	private List<String> pageList  ;
	private List<ContentXmlBean> contentXmlBeanList;
	private Integer maxUnitID;
	private List<String> physicalBoxList;
	private PublicResXmlBean publicRes;
	private DocumentResBean documentRes;
	private String path;
}
