package com.ofd.soft.xml.bean;

import java.util.List;

import lombok.Data;


/**
 * 文档中图片对象
 * @ClassName:  PathObjectBean   
 * @Description:TODO(描述这个类的作用)   
 * @author: 李德才
 * @date:   2020年4月18日 下午1:10:37
 */


@Data
public class PathObjectBean {
	private List<Double> boundary;
	private Boolean fill;
	private String rule;
	private Boolean stroke;
	private List<Integer> fillColor;
	private String abbreviatedData;
}
