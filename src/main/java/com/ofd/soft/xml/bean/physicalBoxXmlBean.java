package com.ofd.soft.xml.bean;

import lombok.Data;



/**
 * 页面定位
 * @ClassName:  physicalBoxXmlBean   
 * @Description:TODO(描述这个类的作用)   
 * @author: 李德才
 * @date:   2020年4月18日 下午1:10:26
 */

@Data
public class physicalBoxXmlBean {
	
	private String value;

}
