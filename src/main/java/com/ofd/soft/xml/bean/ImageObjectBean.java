package com.ofd.soft.xml.bean;

import lombok.Data;

@Data
public class ImageObjectBean {

//	定位
	private String boundary;
	private String cim;
//	标签ID
	private String id;
//	图片资源ID
	private String resourceID;
//	图片Base64
	private String imageBase64;

}
