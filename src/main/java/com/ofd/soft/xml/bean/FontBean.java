package com.ofd.soft.xml.bean;

import lombok.Data;


/**
 * 字体文件
 * @ClassName:  FontBean   
 * @Description:TODO(描述这个类的作用)   
 * @author: 李德才
 * @date:   2020年4月18日 下午1:10:05
 */

@Data
public class FontBean {
	private String fontName;
	private String id;
	private String fontFile;
	private String fontFilePath;

}
