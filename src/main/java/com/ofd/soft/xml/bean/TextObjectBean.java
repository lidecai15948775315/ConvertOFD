package com.ofd.soft.xml.bean;

import lombok.Data;


/** 
 * 文本属性
 * @ClassName:  TextObjectBean   
 * @Description:TODO(描述这个类的作用)   
 * @author: 李德才
 * @date:   2020年4月5日 下午1:39:12
 */
@Data
public class TextObjectBean {
//	填充颜色,默认为黑色
	private String fillColor;
	private Integer codeCount;
	private Integer codePosition;
	private Integer glyphCount;
	private Integer glyphs;
	private String text;
	private Integer fontId;
	private Double fontSize;
	private String fontName;
	private Double X;
	private Double Y;
	private String  boundary;
}
