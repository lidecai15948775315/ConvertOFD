package com.ofd.soft.xml.bean;

import java.util.List;

import lombok.Data;

@Data
public class DocumentResBean {
	
	List<MultiMediaBean> multiMediaList;
	String path;

}
