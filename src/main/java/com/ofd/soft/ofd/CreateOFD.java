package com.ofd.soft.ofd;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.jupiter.api.Test;
import org.ofdrw.font.Font;
import org.ofdrw.layout.OFDDoc;
import org.ofdrw.layout.element.AFloat;
import org.ofdrw.layout.element.Paragraph;
import org.ofdrw.layout.element.Span;

public class CreateOFD {

	private static final String FONT_FILE = "C:\\Windows\\Fonts";
	private static final String TITLE = "中国人民是不可战胜的";
	private static final String CONTENT_1 = "总是在最初的青春挥霍着岁月的生命，总是在最后的时光忆起最初的懈怠";
	private static final String CONTENT_2 = "时光一去不复返，生命耗费不再";
	private static final String CONTENT_3 = "愿你能珍爱生命，珍惜时间！";
	private static final String CONTENT = CONTENT_1 + CONTENT_2 + CONTENT_3;

	@Test
	public void createOfd() throws IOException {
		Path path = Paths.get("C:\\Users\\16233\\Desktop\\DEMO.ofd").toAbsolutePath();
		try (OFDDoc ofdDoc = new OFDDoc(path)) {
//			创建标题
			Span title = new Span(TITLE).setFontSize(7.35d);
			title.setFont(new Font("仿宋", Paths.get(FONT_FILE)));
			Paragraph p = new Paragraph().add(title);
			p.setFloat(AFloat.center);
			ofdDoc.add(p);
//				创建内容
			Span content = new Span(CONTENT).setFontSize(5.29d);
			content.setFont(new Font("黑体", Paths.get(FONT_FILE)));
			p = new Paragraph().add(content);
			p.setFloat(AFloat.center).setMargin(5d);
			ofdDoc.add(p);

		}
	}
}