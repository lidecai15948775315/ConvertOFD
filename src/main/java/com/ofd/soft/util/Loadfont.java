package com.ofd.soft.util;

import java.awt.Font;
import java.io.File;
import java.io.FileInputStream;


/**
 * 获取字体名称
 * @ClassName:  Loadfont   
 * @Description:TODO(描述这个类的作用)   
 * @author: 李德才
 * @date:   2020年4月18日 下午1:47:01
 */

public class Loadfont {
//	private static final String filePath = "C:\\Windows\\Fonts\\STXINGKA.TTF";
	private static final String filePath = "D:\\demoFolder\\STXINGKA.ttf";

	public static void main(String[] args) {
		java.awt.Font loadFont = loadFont(filePath);
		System.err.println(loadFont.getFontName());
	}

	public static Font loadFont(String filePath) {
		try {
			File file = new File(filePath);
			FileInputStream aixing = new FileInputStream(file);
			Font dynamicFont = Font.createFont(Font.TRUETYPE_FONT, aixing);
			aixing.close();
			return dynamicFont;
		} catch (Exception e) {
//			出现异常，默认宋体
			System.err.println("检测失败");
			return  new java.awt.Font("宋体", Font.PLAIN, 14);
		}
	}

}